

    from __future__ import print_function
    from fieldpy.primitives.point import pointCart2D, pointCart3D

## `point` module

---

### Initialization/Construction & Printing

`pointCart2D` and `pointCart3D` objects can be initialized with a `list`,
`tuple`, or `numpy.ndarray` object containing the desired coordinates.


    p0 = pointCart3D([4.0, 1.5, 3.0])
    p1 = pointCart3D([5.0, 2.0, 4.0])

The `(x, y, z)` coordinates (only `(x, y)` for `pointCart2D` objects) store the
cartesian coordinates as attributes


    print("p0.x = ", p0.x)
    print("p0.y = ", p0.y)
    print("p0.z = ", p0.z)

    p0.x =  4.0
    p0.y =  1.5
    p0.z =  3.0


The `pointCart2D` and `pointCart3D` classes include `__repr__` and `__str__`
overloads for easy printing of all coordinates


    print("p0 = ", p0)

    p0 =  (4.0000, 1.5000, 3.0000)


In addition, `pointCart2D` and `pointCart3D` objects can also be initialized
from objects of the same class, acting as a copy-constructor.


    p2 = pointCart3D(p1)
    print ("p2 = ", p2)

    p2 =  (5.0000, 2.0000, 4.0000)


These objects can be quickly converted to a `list` or `tuple` object through the
`toList` and `toTuple` methods


    print(p0.toList())
    print(p1.toTuple())

    [4.0, 1.5, 3.0]
    (5.0, 2.0, 4.0)


---

### Operator Overloading

The `pointCart2D` and `pointCart3D` classes overload all basic mathematical
operators


    print("p0 = ", p0)
    print("p1 = ", p1)
    
    print("p0 + p1 = ", p0+p1)
    print("p0 - p1 = ", p0-p1)
    print("p0 * p1 = ", p0*p1)
    print("p0 / p1 = ", p0/p1)

    p0 =  (4.0000, 1.5000, 3.0000)
    p1 =  (5.0000, 2.0000, 4.0000)
    p0 + p1 =  (9.0000, 3.5000, 7.0000)
    p0 - p1 =  (-1.0000, -0.5000, -1.0000)
    p0 * p1 =  (20.0000, 3.0000, 12.0000)
    p0 / p1 =  (0.8000, 0.7500, 0.7500)


These overloads can be applied between two `pointCart2D`/`pointCart3D` objects
or between a `pointCart2D`/`pointCart3D` and a `list`, `tuple`, or even a single
number


    print("p0 = ", p0)
    print("p1 = ", p1)
    
    print("p0 + 5.0 = ", p0+5.0)
    print("p1 - [1.0, 2.0, 3.0] = ", p1-[1.0, 2.0, 3.0])
    print("p0 * (2.0, 1.5, 10.0) = ", p0*(2.0, 1.5, 10.0))

    p0 =  (4.0000, 1.5000, 3.0000)
    p1 =  (5.0000, 2.0000, 4.0000)
    p0 + 5.0 =  (9.0000, 6.5000, 8.0000)
    p1 - [1.0, 2.0, 3.0] =  (4.0000, 0.0000, 1.0000)
    p0 * (2.0, 1.5, 10.0) =  (8.0000, 2.2500, 30.0000)


---

### Static Methods

The `pointCart2D` and `pointCart3D` classes come with static methods
